
# Table of Contents

1.  [About](#org2ffc905)
2.  [Steps](#org37e0243)



<a id="org2ffc905"></a>

# About

The following project code is created from the below serverless tutorial.

<https://serverless.com/blog/make-serverless-graphql-api-using-lambda-dynamodb/>

It uses serverless framework to create a lambda function that relies on DynamoDB>


<a id="org37e0243"></a>

# Steps

1.  Install nodejs then serverless through npm.

2.  Create an AWS Lab project and add user `serverless_user` in
    IAM. For simplicity, give it the administrator role.

3.  You'll also need to set up API credentials for the user and add
    them to your `~/.aws/credentials`

4.  Try `serverless --version` and make sure that serverless is installed and ready

5.  Run `serverless deploy`.  Serverless will run and create the lambda
    function, DynamoDB table and other resources

